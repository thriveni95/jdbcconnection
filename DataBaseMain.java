import java.util.Scanner;

/**
 * This is a class to implement all database operations
 * 
 * @author ayellapu
 *
 */
public class DataBaseMain {
	public static void main(String[] args) {
		System.out.println("************MYSQL DATABASE ************");
		System.out.println("1.Create the table");
		System.out.println("2.Insert data into the table");
		System.out.println("3.update data into table by ID ");
		System.out.println("4.Delete record from database by ID");
		System.out.println("5.Delete The Table");
		System.out.println("6.Get record by ID");
		System.out.println("7.Delete table");
		System.out.println("8.Count all record");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter your choice");
		int ch = sc.nextInt();
		switch (ch) {
		case 1:
			/*
			 * call a method to create the table in database
			 */
			String tablename = "CREATE TABLE EMP " + "(Emp_id INTEGER , " + " Emp_firstname VARCHAR(255), "
					+ " Emp_lastname VARCHAR(255), " + " Emp_age INTEGER)";
			DataBaseOperations createtabelobj = new DataBaseOperations();
			createtabelobj.createTable(tablename);
			break;
		case 2:
			/*
			 * call a method to insert record into the table
			 */
			String adddata = "INSERT INTO EMP(Emp_id,Emp_firstname,Emp_lastname,Emp_age)"
					+ "VALUES(1,'Thriveni','Chavala',23)";
			DataBaseOperations insertdataobj = new DataBaseOperations();
			insertdataobj.insertData(adddata);
			break;
		case 3:
			/*
			 * call a method to update data by id
			 */
			DataBaseOperations updatedataobj = new DataBaseOperations();
			updatedataobj.updateData("EMP", 1);
			break;
		case 4:
			/*
			 * call a method delete record by id
			 * 
			 */
			DataBaseOperations deletedataobj = new DataBaseOperations();
			// deletedataobj.deleteData("EMPLOYEE",2)
			break;
		/*
		 * call a method to create the table in database
		 */
		case 5:
			/*
			 * call a method delete table by using table name
			 */
			String tablename1 = "DROP TABLE EMP ";
			DataBaseOperations deleteobj = new DataBaseOperations();
			deleteobj.deleteTable(tablename1);
			break;
		case 6:
			/*
			 * call a method get all data by id using id and table name
			 */
			DataBaseOperations getdata = new DataBaseOperations();
			getdata.getDataById("EMP", 1);

			break;
		}

		sc.close();
	}
}
