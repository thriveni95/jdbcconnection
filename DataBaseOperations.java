import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This is a class to perfoem the database operations on the table
 * 
 * @author tchavala
 *
 */
public class DataBaseOperations {

	/*
	 * helper method to create the table
	 */
	public void createTable(String tablename) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {

			stmt.executeUpdate(tablename);
			System.out.println("Created table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * helper method to insert data into the database
	 */

	public void insertData(String adddata) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {

			stmt.executeUpdate(adddata);
			System.out.println("data inserted table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * helper method to update data by id
	 */
	public void updateData(String tablename, int id) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {
			String updatedata = "UPDATE " + tablename + " SET Emp_id=3 where Emp_id= " + id + " ;";
			stmt.executeUpdate(updatedata);
			System.out.println("data updated in table successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/*
	 * helper method to create the table
	 */
	public void deleteTable(String tablename) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {

			stmt.executeUpdate(tablename);
			System.out.println("Deleted table in given database successfully.....");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * helper method to create the table
	 */
	public void getDataById(String tablename, int id) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {

			ResultSet rs = stmt.executeQuery("SELECT * FROM " + tablename + " where Emp_id= " + id + " ;");
			while (rs.next()) {
				System.out.println("Employee id          :" + rs.getInt("EMP_ID"));
				System.out.println("Employee first name   :" + rs.getString("EMP_firstname"));
				System.out.println("Employee last name     :" + rs.getString("EMP_lastname"));
				System.out.println("Employee age           :" + rs.getString("EMP_age"));
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}

	}

}
